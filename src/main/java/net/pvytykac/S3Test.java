package net.pvytykac;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.GetFederationTokenRequest;

public class S3Test {

	public static void main(String[] args) throws Exception {
		String bucket = "pvytykac-test-bucket";

		// s3 data for test
		// bucket/Alice/1.png -> test/resources/1.png
		// bucket/Bob/1.png -> test/resources/2.png

		// requires: s3:*, sts:GetFederationToken
		AdminClient adminClient = new AdminClient("<removed>", "<removed>");

		// uses admin client to get temporary credentials for customer's sub-folder
		ReadOnlyClient readOnlyClient = new ReadOnlyClient(adminClient, bucket);

		readOnlyClient.getObjectMetadata("Alice", "Alice/1.png"); // ok
		readOnlyClient.getObjectMetadata("Bob", "Bob/1.png");     // ok
		readOnlyClient.getObjectMetadata("Alice", "Bob/1.png");   // access denied
		readOnlyClient.getObjectMetadata("Bob", "Alice/1.png");   // access denied
	}

	public static class AdminClient {

		private final AWSSecurityTokenServiceClient stsClient;

		public AdminClient(String accessKey, String secretKey) {
			AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
			this.stsClient = new AWSSecurityTokenServiceClient(credentials);
		}

		public AWSCredentials getTemporaryCredentials(String bucket, String customerId) throws Exception {
			// resource id of the customer's sub-folder
			String resource = "arn:aws:s3:::" + bucket + "/" + customerId + "/*";

			// policy statement - Allow s3::GetObject on the above resource
			Statement statement = new Statement(Statement.Effect.Allow)
					.withActions(S3Actions.GetObject)
					.withResources(new Resource(resource));

			GetFederationTokenRequest request = new GetFederationTokenRequest()
					.withDurationSeconds(900)
					.withName(customerId)
					.withPolicy(new Policy().withStatements(statement).toJson());

			Credentials credentials = stsClient.getFederationToken(request).getCredentials();
			return new BasicSessionCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey(),
					credentials.getSessionToken());
		}
	}

	public static class ReadOnlyClient {

		private final AdminClient adminClient;
		private final String bucket;

		public ReadOnlyClient(AdminClient adminClient, String bucket) {
			this.adminClient = adminClient;
			this.bucket = bucket;
		}

		public void getObjectMetadata(String customerId, String key) throws Exception {
			AWSCredentials credentials = adminClient.getTemporaryCredentials(bucket, customerId);

			try {
				new AmazonS3Client(credentials).getObjectMetadata(bucket, key);
				System.out.println(customerId + ": " + key + " -> ok");
			} catch (Exception ex) {
				System.out.println(customerId + ": " + key + " -> failed");
			}
		}
	}

}
